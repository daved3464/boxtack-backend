import dotenv from 'dotenv'
import mongoose, { connect } from 'mongoose'

dotenv.config()

/* const user = process.env.SERVER_USER;
const pass = process.env.SERVER_PASS;
const host = process.env.SERVER_HOST;
const bd = process.env.SERVER_DB;

const ServerString =
  user.length > 0 ? `mongodb://${user}:${pass}@${host}:27017/${bd}` : host; */

mongoose.connection.on('connecting', () => {
  console.log('connecting to MongoDB...')
})

mongoose.connection.on('error', (error) => {
  console.error('Error in MongoDb connection: ' + error)
  mongoose.disconnect()
})
mongoose.connection.on('connected', () => {
  console.log('MongoDB connected!')
})
mongoose.connection.once('open', () => {
  console.log('MongoDB connection opened!')
})
mongoose.connection.on('reconnected', () => {
  console.log('MongoDB reconnected!')
})
mongoose.connection.on('disconnected', async () => {
  console.log('MongoDB disconnected!')
  await MongoConnect()
})

export default async function MongoConnect() {
  const ServerUri = `mongodb://localhost:27017`

  await connect(ServerUri, {
    dbName: 'roomstack',
    useNewUrlParser: true,
    useUnifiedTopology: true,
    readPreference: 'primary',
    appname: 'Roomstack',
    ssl: false,
    useFindAndModify: false,
    autoReconnect: true,
    serverSelectionTimeoutMS: 5000,
  })
}
