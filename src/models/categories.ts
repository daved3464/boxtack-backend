import { Schema, model, Document } from 'mongoose'

interface ICategory extends Document {
  icon: string
  color: string
  name: string
  user_id?: string
}

const categorySchema = new Schema<ICategory>(
  {
    icon: { type: String },
    color: { type: String },
    name: { type: String, required: true },
    user_id: { type: String },
  },
  {
    collection: 'categories',
  }
)

export const Category = model<ICategory>('Category', categorySchema)
