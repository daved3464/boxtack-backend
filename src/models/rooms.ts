import { Schema, model, Document } from 'mongoose'
import { IItem, IContainer, IRoom } from '../types'

const itemSchema = new Schema<IItem>({
  name: { type: String, required: true },
  quantity: { type: Number, required: true, default: 0 },
  date_acquired: { type: Date, default: Date.now },
  category: { type: String, default: '' },
})

const containerSchema = new Schema<IContainer>({
  name: { type: String, required: true },
  categories: [String],
  items: [itemSchema],
})

const roomSchema = new Schema<IRoom>(
  {
    name: { type: String, required: true },
    type: { type: String, required: true },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    containers: [containerSchema],
    items: [itemSchema],
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  }
)

export const Item = model<IItem>('Item', itemSchema)
export const Container = model<IContainer>('Container', containerSchema)
export const Room = model<IRoom>('Room', roomSchema)
