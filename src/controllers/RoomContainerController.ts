import { QueryOptions } from 'mongoose'
import { IContainer } from '../types'
import { Room, Container } from '../models/rooms'

const idMatch = /^[0-9a-fA-F]{24}$/
const defaultOptions: QueryOptions = { maxTimeMS: 500, returnOriginal: false }

export async function CreateRoomContainer(req: any, res: any) {
  const roomID = req.params.id
  const body = req.body

  try {
    if (!roomID.match(idMatch)) {
      throw new Error('Invalid ID')
    } else {
      const container = new Container(body)

      Room.findByIdAndUpdate(
        roomID,
        { $push: { containers: container } },
        defaultOptions,
        (err) => {
          if (err) throw new Error('Container not Created')

          res.json({
            status: 'success',
            message: 'Container Created',
            content: container,
          })
        }
      ).maxTimeMS(150)
    }
  } catch (e) {
    res.json({
      error: e.message,
    })
  }
}

export async function UpdateRoomContainer(req: any, res: any) {
  const roomID = req.params.id
  const containerID = req.body._id

  try {
    if (!(roomID.match(idMatch) && containerID.match(idMatch))) {
      throw new Error('Ivalid ID')
    } else {
      const update = JSON.parse(`{"containers.$[target].${req.body.prop}": "${req.body.value}"}`)

      await Room.findByIdAndUpdate(
        roomID,
        { $set: update },
        { ...defaultOptions, arrayFilters: [{ 'target._id': { $eq: containerID } }] },
        (err: any, response) => {
          if (err) throw new Error('Container Not Updated')

          if (response !== undefined) {
            res.json({
              status: 'success',
              message: 'Container Updated',
              content: response.containers.filter((container: IContainer) =>
                container._id.equals(containerID)
              ),
            })
          } else {
            res.json({
              status: 'error',
              message: 'Room not Found',
            })
          }
        }
      )
    }
  } catch (e: any) {
    res.json({
      error: e.message,
    })
  }
}

export async function DeleteRoomContainer(req: any, res: any) {
  const roomID = req.params.id
  const containerID = req.params.container_id

  try {
    if (!(roomID.match(idMatch) && containerID.match(idMatch))) {
      throw new Error('Ivalid ID')
    } else {
      await Room.findByIdAndUpdate(
        roomID,
        { $pull: { containers: { _id: containerID } } },
        defaultOptions,
        (err: any, response) => {
          if (err) throw new Error('Document Not Updated')

          if (response !== undefined) res.json({ status: 'success', message: 'Container Deleted' })
          else throw new Error('Room not Found')
        }
      )
    }
  } catch (e: any) {
    res.json({
      error: e.message,
    })
  }
}
