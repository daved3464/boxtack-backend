import { Room } from "../models/rooms";

const idMatch = /^[0-9a-fA-F]{24}$/;

export const FindRooms = async (req: any, res: any) => {
  await Room.find((err, rooms) => {
    res.json(rooms);
  });
};

export const FindRoom = async (req: any, res: any) => {
  const roomID = req.params.id;
  try {
    if (!roomID.match(idMatch)) {
      throw new Error("Invalid ID");
    } else {
      const room = await Room.findById(roomID).maxTimeMS(150);
      if (!room) throw new Error("Room not found");
      res.json(room);
    }
  } catch (e) {
    res.json({
      error: e.message,
    });
  }
};

export const CreateRoom = async (req: any, res: any) => {
  const data = req.body;

  const room = new Room({
    name: data.name,
    type: data.type,
  });

  room
    .save()
    .then((new_room) => {
      res.json(new_room);
    })
    .catch((e: Error) => {
      res.json({
        error: e,
      });
    });
};

export const UpdateRoom = async (req: any, res: any) => {
  const _id: string = req.params.id;
  const body = req.body;

  try {
    if (!_id.match(idMatch)) {
      throw new Error("Invalid ID");
    } else {
      const update = JSON.parse(`{"${req.body.prop}" : "${body.value}" }`);
      await Room.findByIdAndUpdate(_id, update, {}, (err, response) => {
        if (err) throw new Error("Room not updated");
        res.json({
          status: "success",
          message: "Room Updated",
        });
      });
    }
  } catch (e) {
    res.json({
      status: "error",
      message: e,
    });
  }
};

export const DeleteRoom = async (req: any, res: any) => {
  const _id: string = req.params.id;
  try {
    if (!_id.match(idMatch)) {
      throw new Error("Invalid ID");
    } else {
      Room.findByIdAndDelete(_id, {}, (err) => {
        if (err) throw new Error("Room not deleted");
        res.json({
          status: "success",
          message: "Room Deleted",
        });
      });
    }
  } catch (e) {
    res.json({
      status: "error",
      message: e,
    });
  }
};
