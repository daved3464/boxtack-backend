import { IItem } from '../types'
import { Room, Item } from '../models/rooms'

const idMatch = /^[0-9a-fA-F]{24}$/

export const CreateRoomItem = (req: any, res: any) => {
  const roomID = req.params.id
  const body = req.body

  try {
    if (!roomID.match(idMatch)) {
      throw new Error('Invalid ID')
    } else {
      const item = new Item(body)

      Room.findByIdAndUpdate(roomID, { $push: { items: item } }, {}, (err, response) => {
        if (err) throw new Error('Document Not Updated')

        res.json({
          status: 'success',
          message: 'Item Created',
          content: item,
        })
      }).maxTimeMS(150)
    }
  } catch (e) {
    res.json({
      error: e.message,
    })
  }
}

export const UpdateRoomItem = async (req: any, res: any) => {
  const roomID = req.params.id
  const itemID = req.body._id

  try {
    if (!(roomID.match(idMatch) && itemID.match(idMatch))) {
      throw new Error('Ivalid ID')
    } else {
      const update = JSON.parse(`{"items.$[elem].${req.body.prop}": "${req.body.value}"}`)

      await Room.findByIdAndUpdate(
        roomID,
        { $set: update },
        { arrayFilters: [{ 'elem._id': { $eq: itemID } }] },
        (err: any, response) => {
          if (err) throw new Error('Document Not Updated')

          if (response !== undefined) {
            res.json({
              status: 'success',
              message: 'Item Updated',
              content: response.items.filter((item: IItem) => {
                return item._id == itemID
              }),
            })
          } else {
            res.json({
              status: 'error',
              message: 'Room not Found',
            })
          }
        }
      )
    }
  } catch (e: any) {
    res.json({
      error: e.message,
    })
  }
}

export const DeleteRoomItem = async (req: any, res: any) => {
  const roomID = req.params.id
  const itemID = req.params.item_id

  try {
    if (!(roomID.match(idMatch) && itemID.match(idMatch))) {
      throw new Error('Ivalid ID')
    } else {
      const update = JSON.parse(`{"items": {"_id": "${itemID}"}}`)

      await Room.findByIdAndUpdate(roomID, { $pull: update }, (err: any, response) => {
        if (err) throw new Error('Document Not Updated')

        if (response !== undefined) {
          res.json({
            status: 'success',
            message: 'Item Deleted',
          })
        } else {
          res.json({
            status: 'error',
            message: 'Room not Found',
          })
        }
      })
    }
  } catch (e: any) {
    res.json({
      error: e.message,
    })
  }
}
