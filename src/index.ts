import dotenv from "dotenv";
import path from "path";
import createError from "http-errors";
import express, { NextFunction, Request, Response } from "express";
import cookieParser from "cookie-parser";
import logger from "morgan";
import cors from "cors";
import index from "./routes/index";
import rooms from "./routes/rooms";
import users from "./routes/users";

dotenv.config();

const app = express();

// Express Plugins
app.use(logger("dev"));
app.use(cookieParser());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

// Routes
app.use("/api", index);
app.use("/api/users", users);

app.use("/api/rooms", rooms);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404, "This endpoint does not exist!"));
});

// error handler
app.use((err: any, req: Request, res: Response) => {
  res.status(err.status || 500);
  res.setHeader("Content-Type", "application/json");
  res.send(
    JSON.stringify({
      error: err.status,
      message: err.message,
    })
  );
});

module.exports = app;
