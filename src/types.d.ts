import mongoose, { Document } from 'mongoose'

export interface IItem extends Document {
  name: string
  quantity: number
  category?: string
  date_acquired?: Date
}

export interface IContainer extends Document {
  name: string
  categories?: mongoose.Types.Array<string>
  items?: mongoose.Types.Array<IItem>
}
export interface IRoom extends Document {
  name: string
  type: string
  created_at: Date
  updated_at: Date
  containers?: mongoose.Types.Array<IContainer>
  items?: mongoose.Types.Array<IItem>
}
