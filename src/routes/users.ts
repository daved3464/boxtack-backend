import { Router } from "express";

/* GET users listing. */

export default Router().get("/", (req, res, next) => {
  const users = [
    {
      name: "User 1",
      data: {
        birthday: "01/08/1999",
        country: "Colombia",
        city: "Cali",
      },
    },
    {
      name: "User 2",
      data: {
        birthday: "02/03/2001",
        country: "Colombia",
        city: "Cali",
      },
    },
  ];

  res.json(users);
});
