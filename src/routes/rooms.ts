import { Router } from 'express'
import { Room, Container, Item } from '../models/rooms'

import conn from '../utilities/mongo_connection'

import {
  FindRooms,
  FindRoom,
  CreateRoom,
  UpdateRoom,
  DeleteRoom,
} from '../controllers/RoomController'

import { CreateRoomItem, UpdateRoomItem, DeleteRoomItem } from '../controllers/RoomItemController'

import {
  CreateRoomContainer,
  UpdateRoomContainer,
  DeleteRoomContainer,
} from '../controllers/RoomContainerController'

import { moveItem } from '../controllers/RoomItemMoveController'

conn()

const RoomRouter = Router()

Room.on('error', (err: Error) => {
  console.log(err)
})

Item.on('error', (err: Error) => {
  console.log(err)
})

Container.on('error', (err: Error) => {
  console.log(err)
})

/** Rooms Routes */
RoomRouter.route('/').get(FindRooms).post(CreateRoom)

/** Room by ID Route */
RoomRouter.route('/:id').get(FindRoom).put(UpdateRoom).delete(DeleteRoom)

/** Rooms Items Routes */
RoomRouter.route('/:id/item').post(CreateRoomItem).put(UpdateRoomItem)
RoomRouter.delete('/:id/item/:item_id', DeleteRoomItem)
RoomRouter.put('/:id/item/move', moveItem)

/** Rooms Container Routes */
RoomRouter.route('/:id/container').post(CreateRoomContainer).put(UpdateRoomContainer)
RoomRouter.delete('/:id/container/:container_id', DeleteRoomContainer)

export default RoomRouter
